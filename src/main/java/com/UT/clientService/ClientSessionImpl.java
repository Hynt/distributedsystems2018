package com.UT.clientService;

import com.UT.DSortServer;
import com.UT.sorting_server.ArrayChunk;
import org.eclipse.jetty.websocket.api.Session;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

public class ClientSessionImpl implements ClientSession {

    private static AtomicLong idGenerator = new AtomicLong();

    private long id;
    private Session webSocket;

    private volatile boolean running = false;
    private ArrayChunk chunk;

    private long lastChunkJobId = -1;

    ClientSessionImpl(Session webSocket) {
        id = idGenerator.getAndIncrement();
        this.webSocket = webSocket;
        //System.out.println("Started new ClientSession id: " + id);
    }

    @Override
    public Integer call() {
        running = true;
        try {
            while (running && DSortServer.getClientService().isChunks() && webSocket.isOpen()) {

                this.chunk = DSortServer.getSortingService().getChunk();
                if (chunk == null) {
                    System.out.println("Client " + id + " got no chunk. Setting chunks to false.");
                    DSortServer.getClientService().setChunks(false);
                    this.running = false;
                    break;
                }
                //System.out.println("CLIENT-------------Found chunk from job: " + chunk.getJobId());

                // Update job client counters on change
                long jobId = chunk.getJobId();
                if (jobId != lastChunkJobId) {
                    if (lastChunkJobId != -1)
                        DSortServer.getClientService().decrementJobClientCount(lastChunkJobId);
                    DSortServer.getClientService().incrementJobClientCount(jobId);
                    lastChunkJobId = jobId;
                }

                send(chunk.getArray());
                //send(chunk.getArray().toString());

                synchronized (this) {
                    wait();
                }

                chunk = null;
            }
        } catch (Exception e) {
            System.err.println("Client " + id + " broke hard.");
            e.printStackTrace();
        }
        if (lastChunkJobId != -1)
            DSortServer.getClientService().decrementJobClientCount(lastChunkJobId);
        running = false;
        return 1;
    }

    @Override
    public void send(String text) {
        try {
            webSocket.getRemote().sendString(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(List<Integer> chunk) {
        ByteBuffer buffer = ByteBuffer.allocate(chunk.size() * Integer.BYTES);
        buffer.order(ByteOrder.BIG_ENDIAN);
        for (Integer i : chunk) {
            buffer.putInt(i);
        }
        //System.out.println("Sending size " + buffer.position() + " buffer: " + buffer.toString());
        buffer.flip();
        try {
            webSocket.getRemote().sendBytes(buffer);
        } catch (IOException e) {
            //System.err.println("Failed to send chunk");
            e.printStackTrace();
        }
    }


    @Override
    public void merge(List<Integer> result) {
        // System.out.println("Want to merge: " + result);
        chunk.setSorted(result);
        DSortServer.getSortingService().merge(chunk);
        synchronized (this) {
            notify();
        }
    }

    @Override
    public void close() {
        //System.out.println("Closing clientSession id: " + id);
        if (chunk != null) {
            DSortServer.getSortingService().release(chunk);
        }
        running = false;
        synchronized (this) {
            notify();
        }
    }

    @Override
    // Not used
    public ArrayList<Integer> sortChunk(ArrayList<Integer> chunk) {

        return null;
    }

    public long getId() {
        return id;
    }

    public boolean isRunning() {
        return running;
    }
}
