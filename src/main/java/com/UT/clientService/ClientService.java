package com.UT.clientService;

import org.eclipse.jetty.websocket.api.Session;

public interface ClientService {

    void init();

    ClientSession getClient(Session socket);

    void addClient(Session session);

    void removeClient(Session session);

    // Maybe
    // void submitChunk(ArrayChunk chunk);

    void setChunks(boolean sorting);

    boolean isChunks();

    int clientCount();

    int clientCountPerJob(long jobId);

    void removeJob(Long jobStatus);

    void incrementJobClientCount(Long jobId);

    void decrementJobClientCount(Long jobId);
}
