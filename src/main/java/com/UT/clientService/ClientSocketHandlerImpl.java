package com.UT.clientService;

import com.UT.DSortServer;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
@WebSocket(maxTextMessageSize = 32000000, maxBinaryMessageSize = 20000000)
public class ClientSocketHandlerImpl implements ClientSocketHandler {

    private final ClientService clientService = DSortServer.getClientService();

    @Override
    @OnWebSocketConnect
    public void connected(Session session) {
        clientService.addClient(session);
        //System.out.println("New client socket connection: " + clientService.getClient(session).getId());
    }

    @Override
    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        //System.out.println("Closed client socket connection: " + clientService.getClient(session).getId() + " Reason: " + reason);
        clientService.removeClient(session);
        session.close();
    }

    @Override
    @OnWebSocketMessage
    public void message(Session session, byte[] bytes, int offset, int length) {

        //System.out.println("Received array from client: " + clientService.getClient(session).getId());
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        IntBuffer intBuffer = buffer.asIntBuffer();
        List<Integer> out = new ArrayList<>();
        while(intBuffer.hasRemaining()) {
            out.add(intBuffer.get());
        }
        clientService.getClient(session).merge(out);
    }

    @OnWebSocketMessage
    public void message(Session session, String text) {
        //System.out.println("Received string from client: " + clientService.getClient(session).getId());
        List<Integer> int_list = Arrays.stream(text.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        //System.out.println("Converted to " + int_list.getClass());
        clientService.getClient(session).merge(int_list);
    }
}
