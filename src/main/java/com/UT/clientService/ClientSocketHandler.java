package com.UT.clientService;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket
public interface ClientSocketHandler {

    @OnWebSocketConnect
    public void connected(Session session);

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason);

    @OnWebSocketMessage
    public void message(Session session, byte[] bytes, int offset, int length);

    @OnWebSocketMessage
    public void message(Session session, String text);
}
