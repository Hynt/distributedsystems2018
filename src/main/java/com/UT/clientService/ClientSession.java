package com.UT.clientService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public interface ClientSession extends Callable<Integer> {

    ArrayList<Integer> sortChunk (ArrayList<Integer> chunk);

    void send(String str);

    void send(List<Integer> chunk);

    void merge(List<Integer> chunk);

    void close();

    long getId();

    boolean isRunning();

}
