package com.UT.clientService;

import com.UT.DSortServer;
import org.eclipse.jetty.websocket.api.Session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ClientServiceImpl implements ClientService {

    public ClientServiceImpl() {
    }

    @Override
    public void init() {

    }

    private ExecutorService threadPool = Executors.newCachedThreadPool();
    private Map<Session, ClientSession> clientSessions = new ConcurrentHashMap<>();
    private volatile boolean chunks = false;

    private Map<Long, AtomicInteger> jobClientCounters = new ConcurrentHashMap<>();


    @Override
    public ClientSession getClient(Session socket) {
        return clientSessions.get(socket);
    }

    @Override
    public void addClient(Session session) {
        ClientSession clientSession = new ClientSessionImpl(session);
        clientSessions.put(session, clientSession);
        DSortServer.getAdminSocketHandler().TotalUserCountChange(clientSessions.size());
        if (chunks) threadPool.submit(clientSession);
    }

    @Override
    public void removeClient(Session session) {
        ClientSession removed = clientSessions.remove(session);
        DSortServer.getAdminSocketHandler().TotalUserCountChange(clientSessions.size());
        removed.close();
    }

    @Override
    public void setChunks (boolean chunks) {
        if (!chunks) {
            this.chunks = false;
            return;
        }
        if (this.chunks) return;
        this.chunks = true;
        System.out.println("Submitting clients");
        for (ClientSession clientSession : clientSessions.values()) {
            System.out.println("Client " + clientSession.getId() + (clientSession.isRunning()?" is already running":" submitted"));
            if (!clientSession.isRunning()) {
                threadPool.submit(clientSession);
            }
        }
    }

    @Override
    public boolean isChunks() {
        return chunks;
    }

    @Override
    public int clientCount() {
        return clientSessions.size();
    }

    @Override
    public int clientCountPerJob(long jobId) {
        AtomicInteger counter = jobClientCounters.get(jobId);
        if (counter == null) {
            counter = new AtomicInteger();
            jobClientCounters.put(jobId, counter);
        }
        return counter.get();
    }


    @Override
    public void removeJob(Long jobId) {
        jobClientCounters.remove(jobId);
    }

    @Override
    public void incrementJobClientCount(Long jobId) {
        AtomicInteger counter = jobClientCounters.get(jobId);
        if (counter == null) {
            counter = new AtomicInteger();
            jobClientCounters.put(jobId, counter);
        }
        DSortServer.getAdminSocketHandler().JobUserCountChange(jobId, counter.incrementAndGet());
    }

    @Override
    public void decrementJobClientCount(Long jobId) {
        AtomicInteger counter = jobClientCounters.get(jobId);
        DSortServer.getAdminSocketHandler().JobUserCountChange(jobId, counter.decrementAndGet());
    }
}
