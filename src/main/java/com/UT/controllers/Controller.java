package com.UT.controllers;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Service;

import java.util.function.Function;
import java.util.function.Supplier;

abstract class Controller {

    public abstract void initRoutes(Service http);

    Route toRouteReq(Function<Request, String> method) {
        return (Request req, Response res) -> method;
    }

    Route toRouteRes(Function<Response, String> method) {
        return (Request req, Response res) -> method;
    }

    Route toRoute(Supplier<String> method) {
        return (Request req, Response res) -> method;
    }

}
