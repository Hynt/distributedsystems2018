package com.UT.controllers;

import spark.Request;
import spark.Response;
import spark.Service;

import java.util.HashMap;
import java.util.Map;

import static com.UT.Utils.ViewUtils.render;

public class TestController extends Controller {

    private static TestController instance = new TestController();

    public static TestController getInstance() {
        return instance;
    }

    @Override
    public void initRoutes(Service http){
        http.get("/hello/controller", this::testMethod);
        http.get("/hello/controller2", toRoute(this::testMethod2));
        http.get("/hello/controller3", toRouteReq(this::testMethod3));
    }

    private String testMethod(Request req, Response res) {

        Map<String, Object> model = new HashMap<>();
        model.put("message", "Hello Mustache from a controller like class!");
        return render(model, "hello.mustache");
    }

    private String testMethod2() {
        return "";
    }

    private String testMethod3(Request req) {
        return "";
    }
}
