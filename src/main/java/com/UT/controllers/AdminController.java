package com.UT.controllers;

import com.UT.DSortServer;
import com.UT.sorting_server.JobData;
import com.UT.sorting_server.JobStatus;
import com.UT.sorting_server.SortingService;
import com.UT.clientService.ClientService;
import spark.Request;
import spark.Response;
import spark.Service;

import javax.servlet.MultipartConfigElement;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.UT.Utils.ViewUtils.render;

public class AdminController extends Controller {

    private final static AdminController instance = new AdminController();
    public static AdminController getInstance() {
        return instance;
    }

    private final SortingService sortingService = DSortServer.getSortingService();
    private final ClientService clientService = DSortServer.getClientService();


    // Should be held in sortingService and accessed via getJobs() method

    // TODO maybe keep track of admins here

    public void initRoutes(Service http){
        http.get("/admin/jobs", this::jobsList);
        http.get("/admin/jobs/:id", this::jobView);
        http.post("/admin/jobs", "multipart/form-data", this::jobAdd);
        http.post("/admin/jobs/update/:id", "multipart/form-data", this::jobUpdate);
        http.get("/admin/jobs/remove/:id", this::jobRemove);

        http.get("/admin/jobs/run/:id", this::jobRun);
        http.get("/admin/jobs/pause/:id", this::jobPause);
        http.get("/admin/jobs/stop/:id", this::jobStop);
        http.get("/admin/jobs/download/:id/:zip", this::jobDownload);

        http.get("/admin", this::serveLanding);
    }

    private String jobRun(Request req, Response res) {
        System.out.println("Job run");

        Long id = GetId(req);

        if (id < 0L) {
            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }

        // TODO: start or continue the job
        // set isRunning to true and isStarted to true
        sortingService.startSort(id);

        //res.redirect("/admin/jobs/" + id);
        return res.toString();
    }
    private String jobPause(Request req, Response res) {
        System.out.println("Job pause");

        Long id = GetId(req);

        if (id < 0L) {
            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }

        // TODO: pause the job
        // set isRunning to false and isStarted to true
        if (!sortingService.getJobIds().contains(id)) {
            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }

        sortingService.pauseJob(id);

        res.redirect("/admin/jobs/" + id);
        return null;
    }
    private String jobStop(Request req, Response res) {
        System.out.println("Job stop");

        Long id = GetId(req);

        if (id < 0L) {
            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }

        if (!sortingService.getJobIds().contains(id)) {
            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }

        // TODO: stop the job
        sortingService.stopJob(id);

        res.redirect("/admin/jobs/" + id);
        return null;
    }

    private String jobDownload(Request req, Response res) {
        System.out.println("Download job data with id " + req.params(":id"));

        String compressingMethod = req.params(":zip");

        Long id = 0L;

        try
        {
            id = Long.parseLong(req.params(":id"));
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("Was unable to parse :id to integer");

            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }


        JobStatus jobStatus = sortingService.getJobStatus(id);
        if (jobStatus == null) {
            System.out.println("Job not found");
            return null;
        }

        if (!jobStatus.isFinished()) {
            System.out.println("Data is not ready yet!");
            return null;
        }

        List<Integer> data = jobStatus.getResult();

        if (data.size() == 0) {
            System.out.println("No data found");
            return null;
        }

        AdminUtils.returnZipFile(id, data, res, compressingMethod);

        System.out.println("Donneeeee");


        return null;
    }

    private String jobsList(Request req, Response res) {

        // TODO these id's must be loaded dynamically.
        // Could also send a list of Job objects.
        List<Long> ids = sortingService.getJobIds();

        // List<Integer> ids = new ArrayList<Integer>();

        List<String> names = new ArrayList<String>();
        List<String> statuses = new ArrayList<String>();
        // int index = 0;
        for (Long id : ids) {
            JobStatus jobStatus = sortingService.getJobStatus(id);
            if (jobStatus != null) {
                statuses.add(jobStatus.getPhase().toString());
                names.add(jobStatus.getJobName());
            }
            // index++;
        }

        // statuses.add(new ArrayList<Long>());
        // int index = 0;
        // for (Long id : ids) {
        //     JobStatus jobStatus = sortingService.getJobStatus(id);
        //     if (jobStatus != null) {
        //         statuses.get(0).add(jobStatus.getId());
        //     }
        //     index++;
        // }

        Map<String, Object> model = new HashMap<>();

        model.put("ids", ids);
        model.put("names", names);
        model.put("statuses", statuses);

        int totalClientCount = clientService.clientCount();
        model.put("totalClientCount", totalClientCount);

        return render(model, "administration/jobs.mustache");
    }

    private String jobView(Request req, Response res) {

        Long id = 0L;

        try
        {
            id = Long.parseLong(req.params(":id"));
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("Was unable to parse :id to integer");

            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }

        System.out.println("View job with id " + id);

        res.status(200);
        
        JobStatus jobStatus = sortingService.getJobStatus(id);
        if (jobStatus == null) {
            res.status(404);
            System.out.println("Job status with job id " + id + " not found.");
            return null;
        }

        Map<String, Object> model = new HashMap<>();
        model.put("id", id);
        model.put("phase", jobStatus.getPhase().toString());
        model.put("progress", jobStatus.getProgressPercent());
        model.put("size", jobStatus.getSize());
        model.put("name", jobStatus.getJobName());

        // TODO: isRunning and isStarted should be taken from the job's status instead of being hard coded.
        model.put("isStarted", jobStatus.isStarted());
        model.put("isRunning", jobStatus.isRunning());
        model.put("isFinished",jobStatus.isFinished());
        
        boolean isChunking = false;
        if (jobStatus.getPhase().toString().equals("Chunking data")) {
            isChunking = true;
        }
        model.put("isChunking", isChunking);

        int clientCount = clientService.clientCountPerJob(id);
        model.put("clientCount", clientCount);

        return render(model, "administration/job.mustache");
    }

    private String jobAdd(Request req, Response res) {

        req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
        Long jobId = 0L;

        try (InputStream input = req.raw().getPart("dataFile").getInputStream()) {
            // The actual List of integers from the text file
            ArrayList<Integer> integerList = AdminUtils.getIntListFromText(input, req, res);

            System.out.println("Creating a job from the list");
            String jobName = req.queryParams("name");
            jobId = sortingService.submitJob(new JobData(integerList, jobName));
            System.out.println("Job " + jobId + " has been added!");
        }
        catch(Exception e) {
            res.status(500);
            e.printStackTrace();
        }

        res.status(200);

        // Redirects to the main job page
        res.redirect("/admin/jobs/" + jobId);
        return null;
    }

    private String jobUpdate(Request req, Response res) {
        Long id = 0L;

        try
        {
            id = Long.parseLong(req.params(":id"));
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("Was unable to parse :id to integer");

            // TODO redirect user to 404 page.
            res.status(404);
            return res.toString();
        }

        req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));

        try (InputStream input = req.raw().getPart("dataFile").getInputStream()) {
            // TODO: at this point we should have the file. the input variable contains all the file information, which can be used for further processing.
            // Initially the file will be in .csv format.
            System.out.println("We have the file " + input.toString());

            // The actual List of integers from the text file
            ArrayList<Integer> integerList = AdminUtils.getIntListFromText(input, req, res);

            System.out.println("Updating a job...");
            // TODO: update the job data
            //jobId = sortingService.submitJob(new JobData(integerList));
            System.out.println("The job has been updated!");
        }
        catch(Exception e) {
            res.status(500);
            e.printStackTrace();
        }

        res.redirect("/admin/jobs/" + id);
        return null;
    }

    private String jobRemove(Request req, Response res) {
        System.out.println("Delete job with id " + req.params(":id"));

        Long id = Long.parseLong(req.params(":id"));

        // TODO delete job here
        sortingService.deleteJob(id);

        res.status(200);
        
        res.redirect("/admin/jobs");
        return null;
    }

    private String serveLanding(Request req, Response res) {

        // model needs to include id's and maybe names(?) of Jobs.
        // I will use these to create a list of jobs that the admin can handle.
        res.redirect("/admin/jobs");
        return null;
    }

    private Long GetId(Request req) {

        // 0 was a valid index
        Long id = -1L;

        try
        {
            id = Long.parseLong(req.params(":id"));
        }
        catch (NumberFormatException nfe)
        {
            System.out.println("Was unable to parse :id to integer");
        }

        return id;
    }
}
