package com.UT.controllers;

import com.UT.user.User;
import com.UT.user.UserList;
import spark.Request;
import spark.Response;
import spark.Service;
import spark.Session;

import java.util.HashMap;
import java.util.Map;

import static com.UT.Utils.ViewUtils.render;

public class ClientController extends Controller {

    private final static ClientController instance = new ClientController();
    public static ClientController getInstance() {
        return instance;
    }

    private UserList users = new UserList();

    public void initRoutes(Service http){
        http.get("/client", this::serveLanding);
        http.post("/client/connect", this::connect);
        // TODO add routes here
    }

    private String serveLanding(Request req, Response res) {
        //System.out.println("New client!");
        // TODO method stub
//        String userId = req.queryParams("id");
//        String userName;
//        if(req.queryParams().contains("name")) {
//            userName = req.queryParams("name");
//        } else {
//            userName = "default";
//        }
//        Session userSession = req.session();
//        User user = new User(userSession, userName, userId);
//        users.update(user);
//        for(User u : users.getUserList()) {
//            System.out.println(u);
//        }
        Map<String, Object> model = new HashMap<>();
        model.put("message", "Hello Mustache from a controller like class!");
        return render(model, "client/client.mustache");
    }

    private String connect(Request req, Response res) {
        // TODO method stub
        return null;
    }

    private String showClients(Request req, Response res) {
        return null;
    }

}
