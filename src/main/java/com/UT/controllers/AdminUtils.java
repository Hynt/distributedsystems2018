package com.UT.controllers;

import org.apache.commons.io.FilenameUtils;
import spark.Request;
import spark.Response;

import javax.servlet.ServletException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.*;

public class AdminUtils {

    public static ArrayList<Integer> getIntListFromText(InputStream input, Request req, Response res) throws IOException, ServletException {
        // TODO: at this point we should have the file. the input variable contains all the file information, which can be used for further processing.
        // Initially the file will be in .csv format.
        System.out.println("We have the file " + input.toString());
        String fileExtension = FilenameUtils.getExtension(req.raw().getPart("dataFile").getSubmittedFileName());

        // Below here is an example how to parse an uploaded .txt file. This works only if the file can be read line by line (.txt file).
        // In the project's root folder there is a file called testData.txt. This can be used for testing when uploading the file. Although, you can use
        // any .txt file you'd like.
        String str = "";

        // The actual List of integers from the text file
        ArrayList<Integer> integerList = new ArrayList<>();
        int currentInt;

        if(fileExtension.equals("txt") || fileExtension.equals("csv")) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                if (input != null) {
                    while ((str = reader.readLine()) != null) {
                        //System.out.println(str);
                        try {
                            currentInt = Integer.parseInt(str);
                            integerList.add(currentInt);
                        } catch (Exception e) {
                            res.status(500);
                            System.err.println("The string (" + str + ") is not a valid digit");
                        }
                    }
                }
            } finally {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if(fileExtension.equals("zip")) {
            ZipInputStream zipInputStream = new ZipInputStream(input);
            ZipEntry entry;
            while((entry = zipInputStream.getNextEntry()) != null) {
                if (!entry.isDirectory()) {
                    System.out.println("entry: " + entry.getName() + ", " + entry.getSize());
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(zipInputStream));
                        while ((str = reader.readLine()) != null) {
                            //System.out.println(str);
                            try {
                                currentInt = Integer.parseInt(str);
                                integerList.add(currentInt);
                            } catch (Exception e) {
                                res.status(500);
                                System.err.println("The string (" + str + ") is not a valid digit");
                            }
                        }
                    } finally {
                        try {
                            input.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (fileExtension.equals("gz")) {
            try (
                GZIPInputStream gzipInputStream = new GZIPInputStream(input);
                Reader reader = new InputStreamReader(gzipInputStream, "UTF-8");
                Writer writer = new StringWriter();
            ) {
                char[] buffer = new char[10240];
                for (int length = 0; (length = reader.read(buffer)) > 0;) {
                    writer.write(buffer, 0, length);
                }
                str = writer.toString();
                for (String line : str.split(System.getProperty("line.separator"))) {
                    try {
                        line = line.trim();
                        currentInt = Integer.parseInt(line);
                        integerList.add(currentInt);
                    } catch (Exception e) {
                        res.status(500);
                        System.err.println("The string (" + line + ") is not a valid digit");
                    }
                }
            } finally {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return integerList;
    }

    public static void returnZipFile(Long id, List<Integer> data, Response res, String compressingMethod) {
        if (compressingMethod.equals("zip") || compressingMethod.equals("gzip")) {
            try {
                String fileName = "job-" + Long.toString(id);

                File file = new File(fileName + ".txt");
                if (!file.exists()) {
                    file.createNewFile();
                }

                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write("");

                for (int i = 0; i < data.size(); i++) {
                    fileWriter.write(data.get(i).toString());
                    fileWriter.write(System.lineSeparator());
                }

                fileWriter.flush();
                fileWriter.close();

                System.out.println("File done");

                // This part just logs the contents of the file. Can be used for debugging.

                // BufferedReader br = new BufferedReader(new FileReader(file));
                // try {
                //     StringBuilder sb = new StringBuilder();
                //     String line = br.readLine();

                //     while (line != null) {
                //         sb.append(line);
                //         sb.append(System.lineSeparator());
                //         line = br.readLine();
                //     }
                //     String everything = sb.toString();
                //     System.out.println(everything);
                // } finally {
                //     br.close();
                // }

                if (compressingMethod.equals("zip")) {
                    res.raw().setContentType("application/octet-stream");
                    res.raw().setHeader("Content-Disposition", "attachment; filename=" + fileName + ".zip");
                    try {
                        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(res.raw().getOutputStream()));
                             BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file))) {
                            ZipEntry zipEntry = new ZipEntry(fileName + ".txt");

                            zipOutputStream.putNextEntry(zipEntry);
                            byte[] buffer = new byte[1024];
                            int len;
                            while ((len = bufferedInputStream.read(buffer)) > 0) {
                                zipOutputStream.write(buffer, 0, len);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    if (file.delete()) {
                        System.out.println("File deleted successfully");
                    } else {
                        System.out.println("Failed to delete the file");
                    }
                }
                if (compressingMethod.equals("gzip")) {
                    res.raw().setContentType("application/octet-stream");
                    res.raw().setHeader("Content-Disposition", "attachment; filename=" + fileName + ".gz");
                    byte[] buffer = new byte[1024];
                    try {
                        GZIPOutputStream gzipOutputStream = new GZIPOutputStream(new BufferedOutputStream(res.raw().getOutputStream()));
                        FileInputStream fileInputStream = new FileInputStream(fileName + ".txt");

                        int bytes_read;

                        while ((bytes_read = fileInputStream.read(buffer)) > 0) {
                            gzipOutputStream.write(buffer, 0, bytes_read);
                        }

                        fileInputStream.close();

                        gzipOutputStream.finish();
                        gzipOutputStream.close();
                    } catch (IOException e) {
                        System.out.println(e.toString());
                    }

                    if (file.delete()) {
                        System.out.println("File deleted successfully");
                    } else {
                        System.out.println("Failed to delete the file");
                    }
                }
            } catch (IOException e) {
                System.out.println(e.toString());
            }
        } else {
            System.out.println("Compressing method should be either zip or gzip");
        }
    }
}
