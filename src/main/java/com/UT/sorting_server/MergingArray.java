package com.UT.sorting_server;

import java.util.List;

public class MergingArray {

    private int mergedTimes = 0;

    private List<Integer> list;
    private final Job parentJob;
    private final boolean isLast;

    public MergingArray(List<Integer> list, Job parentJob) {
        this(list, parentJob,false, 0);
    }

    public MergingArray(List<Integer> list, Job parentJob, boolean isLast) {
        this(list, parentJob, isLast, 0);
    }

    public MergingArray(List<Integer> list, Job parentJob, boolean isLast, int mergedTimes) {
        this.mergedTimes = mergedTimes;
        this.list = list;
        this.parentJob = parentJob;
        this.isLast = isLast;
    }

    public List<Integer> getList() {
        return list;
    }

    public int getOrder() {
        return mergedTimes;
    }

    public Job getParentJob() {
        return parentJob;
    }

    public boolean isLast() {
        return isLast;
    }
}