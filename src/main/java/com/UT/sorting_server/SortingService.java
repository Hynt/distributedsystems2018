package com.UT.sorting_server;

import java.util.List;

public interface SortingService {

    void init();

    // Job handling

    // Create a new job instance and store it in memory
    // Returns ID of the submitted job
    Long submitJob(JobData data);

    // Start the sorting process on an existing job
    void startSort(Long jobId);

    // Request data about a single job by id
    // Returns JobStatus object that should contain all available info
    JobStatus getJobStatus(Long jobId);

    // Request id of all stored jobs regardless of job state
    // Returns list of jobIds
    List<Long> getJobIds();

    // Stop processing this job but keep progress and references
    void pauseJob(Long id);

    // Remove the job
    void deleteJob(Long id);

    // Remove the job
    void stopJob(Long id);



    // Chunk handling

    //when client is ready, server asks for a chunk for the client
    //sets scale by default to 5 - average power
    // Returns a reference to the chunk to be sorted. Called by clientService.
    ArrayChunk getChunk();

    //scale denotes how powerful client is.
    //Values are in the range [1;10]
    ArrayChunk getChunk(int scale);

    //after the client sorts, server pasHHses the chunk to merge it
    // Reference to chunk and the sorting result directly
    void merge(ArrayChunk chunk);

    //release chunk if the client goes offline or whatever happens
    // Release the chunk to be processed by some other client
    void release(ArrayChunk chunk);

}
