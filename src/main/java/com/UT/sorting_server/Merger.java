package com.UT.sorting_server;

public interface Merger {

    void merge(ArrayChunk chunk, Job job);
}
