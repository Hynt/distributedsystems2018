package com.UT.sorting_server;

import java.util.ArrayList;

public interface DataPool {

    ArrayChunk chunk();
    void release(ArrayChunk chunk);
    void merge(ArrayChunk chunk);
}
