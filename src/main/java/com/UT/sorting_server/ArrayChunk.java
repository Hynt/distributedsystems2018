package com.UT.sorting_server;

import java.util.ArrayList;
import java.util.List;

public class ArrayChunk {

    private Long id;
    private int start;
    private List<Integer> array;
    private List<Integer> sortedArray;
    private final long jobId;
    private int chunkId;

    protected ArrayChunk(int start, List<Integer> array, long jobId, int chunkId) {
        this.start = start;
        this.array = array;
        this.jobId = jobId;
        this.chunkId = chunkId;
    }

    public int getStart() {
        return start;
    }

    public List<Integer> getArray() {
        return array;
    }

    public void setSorted(List<Integer> array) {
        this.sortedArray = array;
    }

    public List<Integer> getSorted() {
        return sortedArray;
    }

    protected Long getId() {
        return this.id;
    }

    public long getJobId() {
        return jobId;
    }

    public int getChunkId() {
        return chunkId;
    }
}
