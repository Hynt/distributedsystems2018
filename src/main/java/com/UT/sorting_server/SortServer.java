package com.UT.sorting_server;

import com.UT.DSortServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.Random;

public class SortServer implements SortingService {

    private static final boolean IS_RANDOM_CHUNKS = false;
    private static final AtomicInteger RANDOM_MODE_INCREMENTOR = new AtomicInteger();

    volatile private boolean isRunning = false;
    private final Map<Long, DataPool> pools = new HashMap<>(10);
    private Object locker = new Object();
    private ExecutorService executorService = new ThreadPoolExecutor(4, 10, 0, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>());
    private ConcurrentHashMap<Long, Job> jobsPool = new ConcurrentHashMap<>();
    private PriorityBlockingQueue<Job> activeJobsQueue = new PriorityBlockingQueue<>();
    private AtomicLong jobId = new AtomicLong(0);
    private final static int DEFAULT_SCALE = 5;
    private final Chunker chunker = new Chunker(executorService);
    private final Merger merger = new HeapMerger(executorService);
    private boolean useParallel = false;

    public SortServer() {
    }

    public void stop() {
        isRunning = false;
    }

    @Override
    public void init() {
    }

    @Override
    public ArrayChunk getChunk() {
        return getChunk(DEFAULT_SCALE);
    }

    @Override
    public ArrayChunk getChunk(int scale) {
        if (IS_RANDOM_CHUNKS) {
            Random random = new Random();

            List<Integer> testArray = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                testArray.add(random.nextInt(100));
            }

            return new ArrayChunk(0, testArray, 1, RANDOM_MODE_INCREMENTOR.getAndIncrement());
        }

        ArrayChunk chunk = null;
        Job job;
        while(chunk == null) {
            job = activeJobsQueue.peek();
            if (job == null) return null; // No active jobs

            chunk = job.getChunk();
            if (chunk == null) activeJobsQueue.remove(job); // Job out of chunks
        }
        return chunk;
    }

    @Override
    public void merge(ArrayChunk chunk) {
        Job job = jobsPool.get(chunk.getJobId());

        //todo: test on server part if ID in chunk is not NULL and only then call merge/release, etc.
        if ( job == null ) return;
        job.addSortedChunk(chunk);
    }

    @Override
    public void release(ArrayChunk chunk) {
        jobsPool.get(chunk.getJobId()).release(chunk);
        System.out.println("Released chunk from job id #" + chunk.getJobId() + " with id #" + chunk.getChunkId());
    }

    @Override
    public Long submitJob(JobData data) {

        // Just increment a serial number and pass it to the Job constructor
        if (data.getArrayToSort().size() == 0) return -1L;
        Long id = jobId.getAndIncrement();
        Job job = new Job(id, data, chunker, useParallel ? new ParallelMerger(executorService) : merger);
        jobsPool.put(id, job);
        job.init();
        return id;
    }

    @Override
    public void startSort(Long jobId) {
        System.out.println("Started job " + jobId);
        Job job = jobsPool.get(jobId);
        if (job != null && !job.getStatus().isRunning()) {
            activeJobsQueue.put(job); //need to come first, otherwise the distribution starts with peek() == null which stops sorting
            job.start();
        }
    }

    @Override
    public JobStatus getJobStatus(Long jobId) {
        Job job = jobsPool.get(jobId);
        if (job != null) return job.getStatus();
        return null;
    }

    @Override
    public List<Long> getJobIds() {
        List<Long> jobIds = new ArrayList<>(this.jobsPool.keySet());
        Collections.sort(jobIds);
        return jobIds;
    }

    @Override
    public void pauseJob(Long id) {
        Job job = jobsPool.get(id);
        if (job != null) {
            activeJobsQueue.remove(job);
            job.pause();
        }
    }

    // Only called on stopped/paused jobs
    @Override
    public void deleteJob(Long id) {
        Job job = jobsPool.get(id);
        activeJobsQueue.remove(jobsPool.get(id));
        jobsPool.remove(id);
        DSortServer.getClientService().removeJob(id);
        DSortServer.getAdminSocketHandler().JobDelete(id);
    }

    @Override
    public void stopJob(Long id) {
        Job oldJob = jobsPool.get(id);
        if (oldJob != null && !oldJob.getStatus().isFinished()) {
            activeJobsQueue.remove(oldJob);
            Job newJob = new Job(id, oldJob.getData(), chunker, oldJob.getMerger());
            jobsPool.put(id, newJob);
            newJob.init();
        }
    }
}
