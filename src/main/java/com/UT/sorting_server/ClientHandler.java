package com.UT.sorting_server;

import java.io.*;

public class ClientHandler implements Runnable {

    private InputStream is;
    private OutputStream os;

    ClientHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    public void run() {
        try {
            read();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void read() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String inputLine, outputLine;

        while ((inputLine = in.readLine()) != null) {
            System.out.println(inputLine);
        }
    }
}
