package com.UT.sorting_server;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

// Class to create chunks from an arrayList of integers
// On creation pass the data and a concurrent container to put chunks into
// call() returns total amount of chunks created from the input data
class ChunkerTask implements Callable<Integer> {

    private final int CHUNK_SIZE; // static chunk size for now
    private final ArrayList<Integer> data;
    private final ConcurrentLinkedQueue<ArrayChunk> resultRef;
    private final Consumer<Integer> callback;
    private final long jobId;

    ChunkerTask(ArrayList<Integer> data, ConcurrentLinkedQueue<ArrayChunk> resultRef, Consumer<Integer> callback, long jobId) {
        this.data = data;
        this.resultRef = resultRef;
        this.callback = callback;
        this.jobId = jobId;
        this.CHUNK_SIZE = Math.max(1500000, (int) Math.sqrt((double) data.size()));
    }

    @Override
    public Integer call() {
        // counters
        int c = 0;
        int chunkCount = 0;

        while(c + CHUNK_SIZE <= data.size()) {
            resultRef.add(
                    new ArrayChunk(
                            c,
                            data.subList(c, c + CHUNK_SIZE),
                            jobId,
                            chunkCount
                    )
            );
            c += CHUNK_SIZE;
            chunkCount++;
        }
        // Handle end piece that can be smaller than CHUNK_SIZE
        if (c < data.size()) {
            resultRef.add(
                    new ArrayChunk(
                            c,
                            data.subList(c, data.size()),
                            jobId,
                            chunkCount
                    )
            );
            chunkCount++;
        }

        callback.accept(chunkCount);
        return chunkCount ; // return total count of chunks
    }
}
