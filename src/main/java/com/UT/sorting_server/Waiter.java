package com.UT.sorting_server;

import java.util.concurrent.*;

public class Waiter implements Runnable {

    private final BlockingQueue<Future> tasksToWait = new LinkedBlockingQueue<>();
    private volatile boolean nonstop = true;

    @Override
    public void run() {

        Future task = null;
        while (nonstop) {
            try {
                task = tasksToWait.take();
                task.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        while ((task = tasksToWait.poll()) != null) {
            try {
                task.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public void waitForTask(Future task) {
        tasksToWait.add(task);
    }

    public void lastWasSubmitted() {
        nonstop = false;
    }
}
