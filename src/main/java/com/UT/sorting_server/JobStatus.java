package com.UT.sorting_server;

import com.UT.DSortServer;

import java.util.List;

import static com.UT.sorting_server.JobStatus.Phase.*;

public class JobStatus {

    public enum Phase {
        INITIAL("Initial state"),
        CHUNKING("Chunking data"),
        WAITING("Ready to start sorting"),
        SORTING("Sorting in progress"),
        PAUSED("Sorting paused"),
        MERGING("Merging results"), // Not needed while parallel
        FINISHED("Sorted successfully!"),
        ERROR("This shouldn't have happened");

        private final String text;

        Phase(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private volatile Phase phase = INITIAL;
    private final long id;
    private int totalCount;
    private volatile int sortedChunkCount; // in chunks
    private volatile int totalChunkCount; // in chunks
    private List<Integer> result;

    // Used in the admin panel to know if the job has been started or if it is running.
    private volatile boolean isStarted = false; // true if job is running or if paused. False if job has never started. False after job stops.
    private volatile boolean isRunning = false; // true if job is running. False if job has never started. False after job stops.

    private final String jobName;

    JobStatus(long id, int size, String jobName) {
        this.id = id;
        this.totalCount = size;
        this.jobName = jobName;
    }

    void init() {
        // Send updates when job started or stopped
        DSortServer.getAdminSocketHandler().JobCreate(id);
        DSortServer.getAdminSocketHandler().JobPhaseChange(id, Phase.INITIAL);
        DSortServer.getAdminSocketHandler().JobProgressChange(id, 0);
    }

    public Phase getPhase() {
        return phase;
    }

    public long getId() {
        return id;
    }

    public String getJobName() {
        return jobName;
    }

    public double getProgressPercent() {
        return ((double)sortedChunkCount / (double)totalChunkCount) * 100;
    }

    public int getSortedChunkCount() {
        return sortedChunkCount;
    }

    public int getTotalChunkCount() {
        return totalChunkCount;
    }

    public int getSize() {
        return totalCount;
    }

    public boolean isDone() {
        return sortedChunkCount == totalChunkCount;
    }

    public List<Integer> getResult() {
        return result;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public boolean isFinished() {
        return phase == Phase.FINISHED;
    }

    private long start = 0;
    private long end = 0;

    void setPhase(Phase newPhase) {
        if (phase == WAITING && newPhase == SORTING) {
            start = System.nanoTime();
            System.out.println("Started counting time for job #" + id);
        } else if (phase == MERGING && newPhase == FINISHED) {
            end = System.nanoTime();
            System.out.println("Job #" + id + " finished in " + (end-start) / 1000000.0 + " ms");
        }
        phase = newPhase;
        DSortServer.getAdminSocketHandler().JobPhaseChange(id, newPhase);
    }

    void setSortedChunkCount(int i) {
        int lastPP = (int) getProgressPercent();
        sortedChunkCount = i;
        int currentPP = (int) getProgressPercent();
        if (lastPP != currentPP) {
            DSortServer.getAdminSocketHandler().JobProgressChange(id, currentPP);
            if (currentPP % 10 == 0) System.out.println("Progress for job " + id + ": " + currentPP + "%");
        }
    }

    void setTotalCount(int i) {
        totalCount = i;
    }

    void setTotalChunkCount(int i) {
        totalChunkCount = i;
    }

    void setResult(List<Integer> result) {
        this.result = result;
    }

    void setStarted(boolean b) {
        isStarted = b;
    }

    void setRunning(boolean b) {
        isRunning = b;
    }

    @Override
    public String toString() {
        return "JobStatus{" +
                "phase=" + phase +
                ", id=" + id +
                ", totalCount=" + totalCount +
                ", sortedChunkCount=" + sortedChunkCount +
                ", totalChunkCount=" + totalChunkCount +
                ", result=" + result +
                ", isStarted=" + isStarted +
                ", isRunning=" + isRunning +
                '}';
    }
}
