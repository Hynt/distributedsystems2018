package com.UT.sorting_server;

import java.util.Comparator;

public class ArrayChunkComparator implements Comparator<ArrayChunk> {

    @Override
    public int compare(ArrayChunk o1, ArrayChunk o2) {
        return o1.getStart() - o2.getStart();
    }
}
