package com.UT.sorting_server;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

// Helper to create and submit chunkerTasks
class Chunker {

    private ExecutorService executorService;

    Chunker(ExecutorService executorService) {
        this.executorService = executorService;
    }

    Future<Integer> chunk(ConcurrentLinkedQueue<ArrayChunk> chunkPool, Job job) {
        ArrayList<Integer> data = job.getData().getArrayToSort();
        return executorService.submit(new ChunkerTask(data, chunkPool, job::chunkingFinishedCallback, job.getId()));
    }
}
