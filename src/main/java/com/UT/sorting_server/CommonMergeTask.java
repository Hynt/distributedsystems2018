package com.UT.sorting_server;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

public class CommonMergeTask extends MergeTask {

    private final MergingArray first;
    private final MergingArray second;
    private final Consumer<MergingArray> callback;

    public CommonMergeTask(ExecutorService executorService, MergingArray first, MergingArray second, Consumer<MergingArray> callback) {
        super(executorService);
        this.first = first;
        this.second = second;
        this.callback = callback;
    }

    @Override
    public Object call() throws Exception {
        List<Integer> resultList = merge(first.getList(), second.getList());
        int order = Math.max(first.getOrder(), second.getOrder()) + 1;
        MergingArray result = new MergingArray(resultList, first.getParentJob(), false, order);
        this.callback.accept(result);
        return null;
    }
}
