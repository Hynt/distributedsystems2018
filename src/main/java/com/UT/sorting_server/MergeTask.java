package com.UT.sorting_server;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

public abstract class MergeTask implements Callable {

    protected final ExecutorService executorService;

    public MergeTask(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public Object call() throws Exception {
        return null;
    }

    protected List<Integer> merge(List<Integer> firstList, List<Integer> secondList) {
        int resultSize = firstList.size() + secondList.size();
        final List<Integer> result = new ArrayList<>(resultSize);
        int firstPos = 0, secondPos = 0;
        while (firstPos < firstList.size() && secondPos < secondList.size()) {
            int elem1 = firstList.get(firstPos);
            int elem2 = secondList.get(secondPos);
            if ( elem1 < elem2 ) {
                result.add(elem1);
                firstPos++;
            } else {
                result.add(elem2);
                secondPos++;
            }
        }
        if (firstPos == firstList.size()) completeMerge(secondList, secondPos, result);
        else completeMerge(firstList, firstPos, result);
        return result;
    }

    private void completeMerge(List<Integer> list, int position, List<Integer> result) {
        for ( int i = position; i < list.size(); i++ ) {
            result.add(list.get(i));
        }
    }
}
