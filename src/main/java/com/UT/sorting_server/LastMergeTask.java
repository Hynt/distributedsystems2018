package com.UT.sorting_server;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

public class LastMergeTask extends MergeTask {

    private final Thread waitingThread;
    private final Waiter waiter;
    private MergingArray mergingArray;
    private final Consumer<List<Integer>> callback;
    private boolean isSorted = false;
    private final ArrayList<ConcurrentLinkedQueue<MergingArray>> orderedSortedList;

    public LastMergeTask(ExecutorService executorService, Thread waitingThread, Waiter waiter, MergingArray mergingArray, Consumer<List<Integer>> callback, ArrayList<ConcurrentLinkedQueue<MergingArray>> orderedSortedList) {
        super(executorService);
        this.waitingThread = waitingThread;
        this.waiter = waiter;
        this.mergingArray = mergingArray;
        this.callback = callback;
        this.orderedSortedList = orderedSortedList;
    }

    @Override
    public Object call() throws Exception {
        waiter.lastWasSubmitted();
        waiter.waitForTask(executorService.submit(()->{}));
        waitingThread.join();

        for ( int i = 0; i < orderedSortedList.size(); i++ ) {
            ConcurrentLinkedQueue<MergingArray> arrays = orderedSortedList.get(i);
            if ( arrays == null || arrays.isEmpty()) continue;
            MergingArray toMerge = arrays.poll();
            int nextOrder = Math.max(mergingArray.getOrder(), toMerge.getOrder())+1;
            mergingArray = new MergingArray(merge(mergingArray.getList(), toMerge.getList()), mergingArray.getParentJob(), true, nextOrder);
        }
        this.callback.accept(mergingArray.getList());
        return null;
    }
}
