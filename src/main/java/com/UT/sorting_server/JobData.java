package com.UT.sorting_server;

import java.util.ArrayList;

public class JobData {

    private ArrayList<Integer> arrayToSort;

    private final String jobName;

    public JobData(ArrayList<Integer> arrayToSort, String jobName) {
        this.arrayToSort = arrayToSort;
        this.jobName = jobName;
    }

    public String getJobName() {
        return jobName;
    }

    ArrayList<Integer> getArrayToSort() {
        return arrayToSort;
    }

    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("[");
        for(int i = 0; i < this.arrayToSort.size() - 1; i++) {
            result.append(this.arrayToSort.get(i).toString());
            result.append(", ");
        }

        result.append(this.arrayToSort.get(this.arrayToSort.size() - 1).toString());
        result.append("]");

        return result.toString();
    }
}
