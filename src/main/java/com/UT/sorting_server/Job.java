package com.UT.sorting_server;

import com.UT.DSortServer;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

class Job implements Comparable<Job>{

    private final long jobId;
    private final JobData jobData;
    private final JobStatus jobStatus;
    private Future<Integer> chunkerResult;
    private int priority;

    private int chunkCount;
    private AtomicInteger sortedChunkCount = new AtomicInteger();
    private final Chunker chunker;
    private final ConcurrentLinkedQueue<ArrayChunk> unsortedChunks = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<List<Integer>> sortedArrays = new ConcurrentLinkedQueue<>();
    private Merger merger;

    private Object boolLock = new Object();
    private boolean[] isSorted;

    private List<Integer> sortedResult;

    Job(long id, JobData jobData, Chunker chunker, Merger merger) {
        this(id, jobData, chunker,merger, 0);
    }

    Job(long id, JobData jobData, Chunker chunker, Merger merger, int priority) {
        this.jobId = id;
        this.jobData = jobData;
        this.chunker = chunker;
        this.jobStatus = new JobStatus(id, jobData.getArrayToSort().size(), jobData.getJobName());
        this.merger = merger;
        this.priority = priority;
    }

    void init() {
        jobStatus.init();
        startChunking();
    }

    long getId() {
        return jobId;
    }

    JobData getData() {
        return jobData;
    }

    int getPriority() {
        return priority;
    }

    public JobStatus getStatus() {
        return jobStatus;
    }

    public List<Integer> getResult() {
        return sortedResult;
    }

    void start() {
        jobStatus.setStarted(true);
        jobStatus.setRunning(true);
        jobStatus.setPhase(JobStatus.Phase.SORTING);
        DSortServer.getClientService().setChunks(true);
    }

    void pause() {
        jobStatus.setRunning(false);
        jobStatus.setPhase(JobStatus.Phase.PAUSED);
    }

    void stop() {
        jobStatus.setStarted(false);
        jobStatus.setRunning(false);
        chunkCount = 0;
        sortedChunkCount = new AtomicInteger();
    }

    private void startChunking() {
        chunkerResult = chunker.chunk(unsortedChunks, this);
        jobStatus.setPhase(JobStatus.Phase.CHUNKING);
    }

    private boolean isChunked() {
        return chunkerResult.isDone();
    }

    void chunkingFinishedCallback(Integer chunkCount) {
        System.out.println("Chunked job " + jobId + " to " + chunkCount + " chunks");
        this.chunkCount = chunkCount;
        isSorted = new boolean[chunkCount];
        for ( int i = 0; i < isSorted.length; i++ ) isSorted[i] = false;
        jobStatus.setPhase(JobStatus.Phase.WAITING);
        jobStatus.setTotalChunkCount(chunkCount);
    }

    void addSortedChunk(ArrayChunk chunk){
        if (!isValidForMerge(chunk)) {
            release(chunk);
            return;
        }
        if (sortedChunkCount.get() == 0) System.out.println("First sorted chunk for job " + jobId);
        isSorted[chunk.getChunkId()] = true;
        sortedArrays.add(chunk.getSorted());
        int count = sortedChunkCount.incrementAndGet();
        jobStatus.setSortedChunkCount(count);
        merger.merge(chunk, this);
    }

    boolean allChunksSubmitted() {
        return isChunked() && sortedChunkCount.get() == chunkCount;
    }

    void mergingFinishedCallback(List<Integer> sortedResult) {
        System.out.println("Merging finished for job " + jobId + " with " + sortedResult.size() + " integers");
        this.sortedResult = sortedResult;
        for (int i = 0; i < sortedResult.size(); i += 1000000) {
            System.out.print(sortedResult.get(i) + " ... ");
        }
        System.out.println();
        jobStatus.setStarted(false);
        jobStatus.setRunning(false);
        jobStatus.setResult(sortedResult);
        jobStatus.setPhase(JobStatus.Phase.FINISHED);
    }

    //to rescue parallel merger from job stopping as it looses reference to it
    public Merger getMerger() {
        return merger;
    }

    void release(ArrayChunk chunk) {
        if (isSorted[chunk.getChunkId()]) return;
        unsortedChunks.add(chunk);
        this.start(); // in case the last chunk was released
    }

    ConcurrentLinkedQueue<List<Integer>> getSortedArrays() {
        return sortedArrays;
    }

    private boolean isValidForMerge(ArrayChunk chunk) {
        return chunk.getSorted()!= null && chunk.getSorted().size() == chunk.getArray().size();
    }

    ArrayChunk getChunk() {
        return unsortedChunks.poll();
    }

    @Override
    public int compareTo(Job other) {
        if (this == other) return 0;
        int result = Integer.compare(this.getPriority(), other.getPriority());
        if (result == 0)
            result = Long.compare(this.getId(), other.getId());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return jobId == job.jobId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId);
    }
}
