package com.UT.sorting_server;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

public class HeapMerger implements Merger {

    private final ExecutorService executorService;

    HeapMerger(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public void merge(ArrayChunk chunk, Job job) {
        if (job.allChunksSubmitted()) {
            job.getStatus().setPhase(JobStatus.Phase.MERGING);
            executorService.submit(new MergeTask(job.getSortedArrays(), job::mergingFinishedCallback));
        }
    }

    private static final class MergeTask implements Callable<Void> {

        private final ConcurrentLinkedQueue<List<Integer>> lists;
        private final Consumer<List<Integer>> callback;

        public MergeTask(ConcurrentLinkedQueue<List<Integer>> lists, Consumer<List<Integer>> callback) {
            this.lists = lists;
            this.callback = callback;
        }

        @Override
        public Void call() {
            callback.accept(merge(lists));
            return null;
        }

        // heap based k-way merge implementation
        // reference https://stackoverflow.com/a/18259218

        private static <E extends Comparable<? super E>> List<E> merge(Collection<? extends List<? extends E>> lists) {
            PriorityQueue<CompIterator<E>> queue = new PriorityQueue<>();
            for (List<? extends E> list : lists)
                if (!list.isEmpty())
                    queue.add(new CompIterator<>(list.iterator()));

            List<E> merged = new ArrayList<>();
            while (!queue.isEmpty()) {
                CompIterator<E> next = queue.remove();
                merged.add(next.next());
                if (next.hasNext())
                    queue.add(next);
            }
            return merged;
        }

        private static class CompIterator<E extends Comparable<? super E>> implements Iterator<E>, Comparable<CompIterator<E>> {
            E peekElem;
            Iterator<? extends E> it;

            CompIterator(Iterator<? extends E> it) {
                this.it = it;
                if (it.hasNext()) peekElem = it.next();
                else peekElem = null;
            }

            @Override
            public boolean hasNext() {
                return peekElem != null;
            }

            @Override
            public E next() {
                E ret = peekElem;
                if (it.hasNext()) peekElem = it.next();
                else peekElem = null;
                return ret;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

            @Override
            public int compareTo(CompIterator<E> o) {
                if (peekElem == null) return 1;
                else return peekElem.compareTo(o.peekElem);
            }

        }
    }
}
