package com.UT.sorting_server;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

public abstract class MergerImpl implements Merger {

    protected final ExecutorService executorService;
    private final ConcurrentHashMap<Long, ConcurrentLinkedQueue<List<Integer>>> listsMap = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<Long, Job> jobsPool;

    MergerImpl(ExecutorService executorService, ConcurrentHashMap<Long, Job> jobsPool) {
        this.executorService = executorService;
        this.jobsPool = jobsPool;
    }
}
