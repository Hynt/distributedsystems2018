package com.UT.sorting_server;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ParallelMerger implements Merger {

    private final ExecutorService executorService;
    private final ArrayList<ConcurrentLinkedQueue<MergingArray>> orderedSortedLists;
    private final Object arrayLock = new Object();
    private final static int INITIAL_ORDER = 0;
    private int mergeCalledCount = 0;
    private final Waiter waiter = new Waiter();
    private final Thread waiterThread;

    ParallelMerger(ExecutorService executorService) {
        this.executorService = executorService;
        this.orderedSortedLists = new ArrayList<>(50);
        this.waiterThread = new Thread(waiter);
        this.waiterThread.setDaemon(true);
        this.waiterThread.start();
    }

    private ConcurrentLinkedQueue<MergingArray> getArrays(int order) {
        ConcurrentLinkedQueue<MergingArray> arrays = null;
        synchronized (arrayLock) {
            while (orderedSortedLists.size() <= order) orderedSortedLists.add(null);
            arrays = orderedSortedLists.get(order);
            if (arrays == null) {
                arrays = new ConcurrentLinkedQueue<>();
                orderedSortedLists.set(order, arrays);
            }
        }
        return arrays;
    }

    @Override
    public synchronized void merge(ArrayChunk chunk, Job job) {
        mergeCalledCount++;
        boolean isLastChunk = mergeCalledCount == job.getStatus().getTotalChunkCount();
        MergingArray mergingArray = new MergingArray(chunk.getSorted(), job, isLastChunk);
        if (mergingArray.isLast()){
            executorService.submit(new LastMergeTask(executorService, waiterThread, waiter, mergingArray, job::mergingFinishedCallback, orderedSortedLists));
            return;
        }
        ConcurrentLinkedQueue<MergingArray> arrays = getArrays(INITIAL_ORDER);
        MergingArray toMerge = arrays.poll();
        if (toMerge != null) executorService.submit(new CommonMergeTask(executorService, mergingArray, toMerge, this::mergeFinished));
        else arrays.add(mergingArray);
    }

    private synchronized void mergeFinished(MergingArray result) {

        ConcurrentLinkedQueue<MergingArray> arrays = getArrays(result.getOrder());
        MergingArray toMerge = arrays.poll();
        if (toMerge != null) {
            Future res = executorService.submit(new CommonMergeTask(executorService, result, toMerge, this::mergeFinished));
            waiter.waitForTask(res);
        } else {
            arrays.add(result);
        }
    }
}
