package com.UT;

import com.UT.adminService.AdminSocketHandler;
import com.UT.adminService.AdminSocketHandlerImpl;
import com.UT.clientService.ClientService;
import com.UT.clientService.ClientServiceImpl;
import com.UT.clientService.ClientSocketHandler;
import com.UT.clientService.ClientSocketHandlerImpl;
import com.UT.controllers.AdminController;
import com.UT.controllers.ClientController;
import com.UT.controllers.TestController;
import com.UT.sorting_server.SortServer;
import com.UT.sorting_server.SortingService;
import spark.Service;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;


public class DSortServer {

    private final static SortingService sortingService = new SortServer();
    private final static ClientService clientService = new ClientServiceImpl();
    private final static AdminSocketHandler adminSocketHandler = new AdminSocketHandlerImpl();
    private final static ClientSocketHandler clientSocketHandler = new ClientSocketHandlerImpl();
    private final static boolean IS_DEMO = false;
    private final static int DEMO_SIZE = 35000000;

    public static void main(String[] args) {

        if (IS_DEMO) {
            long start = System.nanoTime();
            ArrayList<Integer> array = new ArrayList<>(DEMO_SIZE);
            int finished = -1;
            try(FileOutputStream fos = new FileOutputStream("/home/turik1997/data-" + DEMO_SIZE + ".txt")){
                final String newLine = System.getProperty("line.separator");
                for ( int i = 0; i < DEMO_SIZE; i++ ) {
                    int val = (int)(Math.random()*Integer.MAX_VALUE);
                    int sign = (int)(Math.random() * 10);
                    val *= sign % 2 == 0 ? 1 : -1;
                    array.add(val);
                    fos.write((val + newLine).getBytes());

                    int progress = (int)((i+1) *100.0/ DEMO_SIZE);
                    if (progress > finished && progress % 10 == 0) {
                        System.out.println("Generating random data: " + progress + "%");
                        finished = progress;
                    }
                }
            }catch (Exception ex) {
                System.out.println(ex);

            }
            long end = System.nanoTime();
            System.out.println("Finished generating random data with " + DEMO_SIZE + " elements in " + (end-start)/1000000.0 + " ms");

            System.out.println("Sorting " + DEMO_SIZE + " elements on the machine itself...");
            start = System.nanoTime();
            Collections.sort(array);
            end = System.nanoTime();
            System.out.println("Sorted " + DEMO_SIZE + " elements by itself in " + (end-start)/1000000.0 + " ms");
        }


        Service http = initSparkServer();
        initSockets(http);
        initFilters(http);
        initRoutes(http);

        sortingService.init();
        //sortingService.start();

        clientService.init();
    }

    private static Service initSparkServer() {

        Service http = Service.ignite();
        http.ipAddress("0.0.0.0").port(8080);
        http.staticFiles.externalLocation("war/www/public");
        http.webSocketIdleTimeoutMillis(Integer.MAX_VALUE);

        return http;
    }

    private static void initSockets(Service http) {
        http.webSocket("/sortingSocket", clientSocketHandler);
        http.webSocket("/adminSocket", adminSocketHandler);
    }

    private static void initFilters(Service http) {
        // Remove trailing slashes
        http.before("*", (request, response) -> {
            if (request.pathInfo().endsWith("/")) {
                response.redirect(request.pathInfo().substring(0, request.pathInfo().length() - 1));
            }
        });
        // Filters here
    }

    private static void initRoutes(Service http) {

        TestController.getInstance().initRoutes(http);
        AdminController.getInstance().initRoutes(http);
        ClientController.getInstance().initRoutes(http);
    }

    public static SortingService getSortingService() {
        return sortingService;
    }

    public static ClientService getClientService() {
        return clientService;
    }

    public static AdminSocketHandler getAdminSocketHandler() {
        return adminSocketHandler;
    }
}
