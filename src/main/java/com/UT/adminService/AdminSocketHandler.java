package com.UT.adminService;

import java.io.InputStream;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import com.UT.sorting_server.JobStatus;
import com.UT.sorting_server.JobStatus.Phase;

// import java.io.InputStream;

@WebSocket
public interface AdminSocketHandler {

    @OnWebSocketConnect
    public void connected(Session session);

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason);

    @OnWebSocketMessage
    public void message(Session session, InputStream stream);

    public void sendToAll(String json);

    public void sendById(Long jobId, String json);

    public void send(Session session, String json);

    public void JobPhaseChange(Long jobId, JobStatus.Phase newPhase);

    public void JobProgressChange(Long jobId, int newPercentage);

    public void JobCreate(Long jobId);

    public void JobDelete(Long jobId);

    public void JobUserCountChange(Long jobId, int userCount);

    public void TotalUserCountChange(int userCount); // should not contain admin users
}
