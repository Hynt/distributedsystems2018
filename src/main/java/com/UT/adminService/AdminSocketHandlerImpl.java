package com.UT.adminService;

import com.UT.DSortServer;
import com.UT.sorting_server.JobStatus;
import com.UT.sorting_server.SortingService;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;


@WebSocket
public class AdminSocketHandlerImpl implements AdminSocketHandler {

    private Map<Long, List<Session>> sessions = new HashMap<Long, List<Session>>();

    private final SortingService sortingService = DSortServer.getSortingService();

    @Override
    @OnWebSocketConnect
    public void connected(Session session) {
        System.out.println("New admin socket connection:" + session.getLocalAddress());
    }

    @Override
    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        System.out.println("Closed admin socket connection: " + session.getLocalAddress() + " Reason: " + reason);
        DeregisterSession(session);
    }

    @Override
    @OnWebSocketMessage
    public void message(Session session, InputStream stream) {
        System.out.println("Received a stream on connection: ");
        try {
            int something = stream.read();
            System.out.println("Read " + something);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnWebSocketMessage
    public void message(Session session, String text) {

        if (session == null) {
            System.out.println("Invalid session. Returning.");
            return;
        }

        System.out.println("Received string on session: " + session.getLocalAddress());

        // The only data the admin needs to send to server is the location. Is the admin
        // currently on the job's list page or is the admin currently viewing a certain job?

        RegisterSession(session, text);
        RefreshSession(session, text);
    }

    void RefreshSession(Session session, String text) {
        Long jobId = -2L;

        try {
            jobId = Long.parseLong(text);
         } catch (NumberFormatException nfe) {
            System.out.println("NumberFormatException: " + nfe.getMessage());
         }

         JobStatus jobStatus = sortingService.getJobStatus(jobId);
         if (jobStatus != null) {
            String phase = jobStatus.getPhase().toString();

            String jsonString = new JSONObject()
            .put("eventName", "OnPhaseChange")
            .put("jobId", jobId.toString())
            .put("phase", phase)
            .toString();

            send(session, jsonString);
         }
    }

    public void sendToAll(String json) {
        System.out.println("Send to all");
        System.out.println(json);
        for (Map.Entry<Long, List<Session>> entry : sessions.entrySet())
        {
            for (Session session : sessions.get(entry.getKey())) {
                if (session != null) {
                    send(session, json);
                }
            }
        }
    }
    public void sendById(Long jobId, String json) {
        //System.out.println("Send by id " + jobId);
        //System.out.println(json);
        List<Session> sessionList = sessions.get(jobId);
        if (sessionList == null) {
            System.err.println("Failed to send update to admins because no listening sessions for job " + jobId);
            return;
        }
        for (Session session : sessionList) {
            send(session, json);
        }
    }
    public void send(Session session, String json) {
        if(json == null || json.isEmpty()) {
            System.out.println("Created json is empty. Event send canceled.");
            return;
        }

        try {
            synchronized (session) {
                session.getRemote().sendString(json);
            }
        } catch (IOException e) {
            System.out.println("sending failed");
            e.printStackTrace();
        }
    }

    @Override
    public void JobPhaseChange(Long jobId, JobStatus.Phase newPhase) {
        String jsonString = new JSONObject()
                .put("eventName", "OnPhaseChange")
                .put("jobId", jobId.toString())
                .put("phase", newPhase.toString())
                .toString();

        sendById(jobId, jsonString);
        sendById(-2L, jsonString);
    }

    @Override
    public void JobProgressChange(Long jobId, int newPercentage) {
        String jsonString = new JSONObject()
                .put("eventName", "OnProgressChange")
                .put("jobId", jobId.toString())
                .put("progress", newPercentage)
                .toString();

        sendById(jobId, jsonString);
    }

    @Override
    public void JobCreate(Long jobId) {
        // rank(?), id, name, status

        JobStatus jobStatus = sortingService.getJobStatus(jobId);

        String jsonString = new JSONObject()
                .put("eventName", "OnJobCreate")
                .put("jobId", jobId.toString())
                .put("name", jobStatus.getJobName())
                .put("phase", jobStatus.getPhase().toString())
                .toString();

        sendById(-2L, jsonString);
    }

    @Override
    public void JobDelete(Long jobId) {
        String jsonString = new JSONObject()
                .put("eventName", "OnJobDelete")
                .put("jobId", jobId.toString())
                .toString();

        sendById(jobId, jsonString);
        sendById(-2L, jsonString);
    }

    @Override
    public void JobUserCountChange(Long jobId, int userCount) {
        String jsonString = new JSONObject()
                .put("eventName", "OnJobUserCountChange")
                .put("jobId", jobId.toString())
                .put("userCount", userCount)
                .toString();

        sendById(jobId, jsonString);
        // sendById(-2L, jsonString);
    }

    @Override
    public void TotalUserCountChange(int userCount) {
        String jsonString = new JSONObject()
                .put("eventName", "OnTotalUserCountChange")
                .put("totalUserCount", userCount)
                .toString();

        sendById(-2L, jsonString);
    }

    private void RegisterSession(Session session, String text) {
        Long jobId = -2L;

        try {
            jobId = Long.parseLong(text);
         } catch (NumberFormatException nfe) {
            System.out.println("NumberFormatException: " + nfe.getMessage());
         }

         if (jobId == -2 || jobId >= 0) { // admin is viewing the jobs list or a concrete valid job
            AddSession(jobId, session);
         }
    }
    private void DeregisterSession(Session session) {
        if (session == null) { System.out.println("Trying to deregister null session. Terminating."); return;}

        RemoveSession(session);
    }

    private void RemoveSession(Session session) {
        if (session == null) { System.out.println("Trying to remove null session. Terminating."); return;}

        for (Map.Entry<Long, List<Session>> entry : sessions.entrySet())
        {
            Long jobId = entry.getKey();

            if (sessions.get(jobId).contains(session)) {
                sessions.get(jobId).remove(session);
                System.out.println("Successfully removed session from entry with jobId " + jobId.toString());
            }
        }
    }

    private void AddSession(Long jobId, Session session) {
        if (session == null) { System.out.println("Trying to add null session. Terminating."); return;}

        if (sessions.containsKey(jobId)) {
            if (sessions.get(jobId) == null) {
                sessions.put(jobId, new LinkedList<Session>());
            }
            if (!sessions.get(jobId).contains(session)) {
                sessions.get(jobId).add(session);
                System.out.println("Added session to list");
            }
        }
        else {
            System.out.println("Map does not contain the " + jobId.toString() + " entry");
            sessions.put(jobId, new LinkedList<Session>());
            sessions.get(jobId).add(session);
            System.out.println("Initialized " + jobId.toString() + " array and added session.");
        }
    }
}
