package com.UT.user;

import java.util.ArrayList;

public class UserList {

    private ArrayList<User> userArrayList = new ArrayList<>();

    public UserList() {

    }

    public void update(User user) {
        if(!(userArrayList.contains(user))) {
            userArrayList.add(user);
        }
    }

    public ArrayList<User> getUserList() {
        return userArrayList;
    }
}
