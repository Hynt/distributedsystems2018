package com.UT.user;

import spark.Session;

public class User {

    private Session session;
    private String userName;
    private String id;

    public User(Session session, String userName, String id) {
        this.session = session;
        this.userName = userName;
        this.id = id;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if(o == this) {
            return true;
        }

        if(!(o instanceof User)) {
            return false;
        }

        User u = (User) o;

        return id.equals(u.getId())
                && userName.equals(u.getUserName())
                && session.id().equals(u.getSession().id());
    }

    @Override
    public String toString(){
        return id + ", " + userName + ", " + session.id();
    }

    public boolean isSameSession(User user) {
        return session.id().equals(user.getSession().id());
    }
}
