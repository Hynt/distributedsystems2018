
let socket,
    connectButton = $("#connectButton"),
    disconnectButton = $("#disconnectButton");


function randomDataSet(dataSetSize, minValue, maxValue) {
    return new Array(dataSetSize).fill(0).map(function(n) {
        return Math.floor(Math.random() * (maxValue - minValue) + minValue);
    });
}
function local_sorting() {
    var random_array = randomDataSet(10, 1, 25);
    var sorted_array =  random_array.slice().sort(function( a , b ){
        return a-b;
    });
    document.getElementById("inArray").innerHTML = random_array.toString();
    document.getElementById("outArray").innerHTML = sorted_array.toString();
    return(sorted_array)
}

function string_sort(data) {


    var a = data.substr(1).slice(0, -1);
    var b = a.split(',').map(Number);
    var c = b.sort(function( a , b ){return a-b;}).toString()


    return[c,b.length]
}

function connect() {
    toggleConnectButtons();

    socket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/sortingSocket");

    socket.onopen = function () {
        console.log("Connected socket");
    };
    socket.onmessage = function (event) {

        console.log ("message from server:", event.data);

        var input = string_sort(event.data);
        var sorted_array = input[0];
        var len = input[1];

        document.getElementById("chunkSize").innerHTML = len.toString();
        // document.getElementById("inArray").innerHTML = event.data;
        // document.getElementById("outArray").innerHTML = "["+ sorted_array + "]" ;
        socket.send(sorted_array);
        counter++;
        document.getElementById("chunkCount").innerHTML = counter.toString();

    };


    socket.onclose = function () {
        console.log("Closed socket");
    };

    var counter = 0;

    console.log(location.hostname);
    console.log(location.port);

}

function disconnect() {
    toggleConnectButtons();
    socket.close();

}

function toggleConnectButtons() {
    connectButton.toggle();
    disconnectButton.toggle();
}
