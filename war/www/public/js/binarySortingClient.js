"use strict";

var chunkCount = 0;
var socket,
    connectButton = $("#connectButton"),
    disconnectButton = $("#disconnectButton");

function sortArrayBuffer(buf) {
    let view = new DataView(buf);

    let array = [];
    for (let viewIndex=0; viewIndex<view.byteLength; viewIndex=viewIndex+4) {
        array.push(view.getInt32(viewIndex, false));
    }
    //$("#inArray").text(array.toString());

    array = array.sort(function( a , b ){return a-b;});

    //$("#outArray").text(array.toString());

    $("#chunkSize").text(array.length.toString());

    array.forEach(function(value, index){
        view.setInt32(index * 4, value, false);
    });

    $("#chunkCount").text(++chunkCount);
    console.log("Finished sorting");
    return buf;
}



function connect() {

    socket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/sortingSocket");
    socket.binaryType = "arraybuffer";

    socket.onopen = function () {
        console.log("Connected socket");
        toggleConnectButtons();
    };
    socket.onmessage = function (event) {
        console.log ("message from server");
        socket.send(sortArrayBuffer(event.data));
    };
    socket.onclose = function () {
        console.log("Closed socket");
        toggleConnectButtons();
        $("#chunkSize").text(""); // Clear last chunk on disconnect
        chunkCount = 0; // Update value but not the interface

    };
    socket.onerror = function(event) {
        console.error("WebSocket error observed:", event);
    };

    console.log(location.hostname);
    console.log(location.port);

}
var counter = 0;

function disconnect() {
    socket.close();
}

function toggleConnectButtons() {
    connectButton.toggle();
    disconnectButton.toggle();
}

