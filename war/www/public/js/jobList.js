
var onPhaseChange = function (eventObj) {
    console.log("We are in onPhaseChange()");

    var jobId = eventObj.jobId;
    if (jobId > -2) {
        var index = GetJobIndexInList(jobId);
        if (index != -1000) {
            var phaseSpan = $('#phases').children().eq(index).children().eq(0);
            ChangeJobPhase(phaseSpan, eventObj.phase);
        }
    }
};
var onJobCreate = function (eventObj) {
    console.log("We are in onJobCreate()");

    var jobId = eventObj.jobId;
    var index = GetJobIndexInList(jobId);
    if (index != -1000) {
        console.log('Job with this id already in the list!');
    }
    else {
        var idEntry = $("<p><span id='id_" + jobId + "'><a href='/admin/jobs/" + jobId + "'>#" + jobId + "</a></span></p>");
        var nameEntry = $("<p><span>" + eventObj.name + "</span></p>");
        var phaseEntry = $("<p><span>" + eventObj.phase + "</span></p>");

        $('#ids').append(idEntry);
        $('#names').append(nameEntry);
        $('#phases').append(phaseEntry);
    }
};
var onJobDelete = function (eventObj) {
    console.log("We are in onJobDelete()");

    var jobId = eventObj.jobId;
    if (jobId > -2) {
        var index = GetJobIndexInList(eventObj.jobId);
        if (index != -1000) {
            var idEntry = $('#ids').children().eq(index);
            var nameEntry = $('#names').children().eq(index);
            var phaseEntry = $('#phases').children().eq(index);

            idEntry.remove();
            nameEntry.remove();
            phaseEntry.remove();
        }
    }
};
var onTotalUserCountChange = function (eventObj) {
    console.log("We are in onTotalUserCountChange()");

    ChangeTotalClientCount(eventObj.totalUserCount);
};

var callbackFunctions = {
    "OnPhaseChange" : onPhaseChange,
    "OnJobCreate" : onJobCreate,
    "OnJobDelete" : onJobDelete,
    "OnTotalUserCountChange" : onTotalUserCountChange
};

var socket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/adminSocket");

socket.onopen = function() {
    socket.send("-2");
};

socket.onmessage = function (event) {
    console.log("Got message from server");
    processMessage(event.data);
};

function processMessage(msg) {
    console.log("Processing message: " + msg);
    var parsedObj = jQuery.parseJSON(msg);

    var eventName = parsedObj.eventName;

    if (eventName in callbackFunctions) {
        callbackFunctions[eventName](parsedObj);
    }
    else {
        console.log(eventName + " is not registered.");
    }
}

function GetJobIndexInList(jobId) {
    var index = -1000;

    var idElement = $('#ids').find('#id_' + jobId);

    if (idElement.length > 0) {
        index = idElement.parent().index();
    }

    return index;
}
function ChangeJobPhase(span, newVal) {
    span.text(newVal);
}
function ChangeTotalClientCount(newVal) {
    $('#totalClientCount').text(newVal);
}