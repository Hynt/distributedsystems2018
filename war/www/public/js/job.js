var progressBar = $('#progressbar');
var logDiv = $('#logDiv');
var logList = $('#logList');

var firstUser = true;
var lastUserCount = 0;

var done_20 = false;
var done_40 = false;
var done_60 = false;
var done_80 = false;
var done_100 = false;

var onPhaseChange = function (eventObj) {
    console.log("We are in onPhaseChange()");
    ChangeJobPhase(eventObj.phase);
};
var onProgressChange = function (eventObj) {
    console.log("We are in onProgressChange()");
    ChangeProgressBar(eventObj.progress);
};
var onJobDelete = function (eventObj) {
    console.log("We are in onJobDelete()");

    SetRunButton(false);
    SetPauseButton(false);
    SetStopButton(false);
    SetDeleteButton(false);

    Log("This job was deleted.");

    alert("Job is deleted. Further actions not possible.");

    CloseSocket();
};
var onJobUserCountChange = function (eventObj) {
    console.log("We are in onJobUserCountChange()");
    SetClientCount(eventObj.userCount);
};

var callbackFunctions = {
    "OnPhaseChange" : onPhaseChange,
    "OnProgressChange" : onProgressChange,
    "OnJobDelete" : onJobDelete,
    "OnJobUserCountChange" : onJobUserCountChange
};

var socket = new WebSocket("ws://" + location.hostname + ":" + location.port + "/adminSocket");

socket.onopen = function() {
    var job_id = $('#id').text();
    socket.send(job_id);
};

socket.onmessage = function (event) {
    console.log("Got message from server");
    processMessage(event.data);
    
};

function processMessage(msg) {
    console.log("Processing message: " + msg);
    var parsedObj = jQuery.parseJSON(msg);

    var eventName = parsedObj.eventName;

    if (eventName in callbackFunctions) {
        callbackFunctions[eventName](parsedObj);
    }
    else {
        console.log(eventName + " is not registered.");
    }
}

$("#runForm").submit(function(e) {

    var id = $('#id').text();
    var url = "/admin/jobs/run/" + id;

    $.ajax({
        type: "GET",
        url: url,
        data: $("#runForm").serialize(),
        success: function(data)
        {
            console.log("Connection is good. Jieee boiiiii");
            SetRunButton(false);
            SetPauseButton(true);
            SetStopButton(true);
            SetDeleteButton(false);
        },
        fail: function (e) {
            console.log("failed");
        },
        always: function() {
            console.log("always");
        }
    });

    e.preventDefault();
});

$("#pauseForm").submit(function(e) {

    var id = $('#id').text();
    var url = "/admin/jobs/pause/" + id;

    $.ajax({
        type: "GET",
        url: url,
        data: $("#pauseForm").serialize(),
        success: function(data)
        {
            console.log("Connection is good. Paused");
            SetRunButton(true);
            SetPauseButton(false);
            SetStopButton(true);
            SetDeleteButton(false);
        },
        fail: function (e) {
            console.log("failed pause");
        },
        always: function() {
            console.log("always pause");
        }
    });

    e.preventDefault();
});

$("#stopForm").submit(function(e) {

    var id = $('#id').text();
    var url = "/admin/jobs/stop/" + id;

    $.ajax({
        type: "GET",
        url: url,
        data: $("#stopForm").serialize(),
        success: function(data)
        {
            console.log("Connection is good. Paused");
            SetRunButton(true);
            SetPauseButton(false);
            SetStopButton(false);
            SetDeleteButton(true);
        },
        fail: function (e) {
            console.log("failed stop");
        },
        always: function() {
            console.log("always stop");
        }
    });

    e.preventDefault();
});

function SetRunButton(newVal) {
    $("#runFormInput").prop('disabled', !newVal);
}
function SetPauseButton(newVal) {
    $("#pauseFormInput").prop('disabled', !newVal);
}
function SetStopButton(newVal) {
    $("#stopFormInput").prop('disabled', !newVal);
}
function SetDeleteButton(newVal) {
    $("#deleteFormInput").prop('disabled', !newVal);
}
function SetDownloadButton(newVal) {
    $("#downloadButton").prop('disabled', !newVal);
}

function ChangeJobPhase(newVal) {
    $('#jobStatus').text(newVal);

    switch (newVal) {
        case "Initial state":
            console.log("INITIAL");

            SetRunButton(false);
            SetPauseButton(false);
            SetStopButton(false);
            SetDeleteButton(true);

            break;
        case "Chunking data":
            console.log("CHUNKING");

            SetRunButton(false);
            SetPauseButton(false);
            SetStopButton(false);
            SetDeleteButton(true);

            break;
        case "Ready to start sorting":
            console.log("READY");

            SetRunButton(true);
            SetPauseButton(false);
            SetStopButton(false);
            SetDeleteButton(true);

            break;
        case "Sorting in progress":
            console.log("SORTING");

            SetRunButton(false);
            SetPauseButton(true);
            SetStopButton(true);
            SetDeleteButton(false);

            break;
        case "Sorting paused":
            console.log("PAUSED");

            SetRunButton(true);
            SetPauseButton(false);
            SetStopButton(true);
            SetDeleteButton(false);

            break;
        case "Merging results":
            console.log("MERGING");

            SetRunButton(false);
            SetPauseButton(false); // kas saab pausida?
            SetStopButton(true);
            SetDeleteButton(false);

            break;
        case "Sorted successfully!":
            console.log("SUCCESS");

            SetRunButton(false);
            SetPauseButton(false);
            SetStopButton(false);
            SetDeleteButton(true);
            SetDownloadButton(true);

            break;
        case "This shouldn't have happened":
            console.log("SHIT");

            SetRunButton(false);
            SetPauseButton(false);
            SetStopButton(false);
            SetDeleteButton(false);

            break;
        default:

            SetRunButton(false);
            SetPauseButton(false);
            SetStopButton(false);
            SetDeleteButton(true);

            console.log("NORMY");
    }

    Log(newVal);
}
function ChangeProgressBar(newVal) {
    progressBar.css('width', newVal+"%");
    progressBar.text(newVal + '%');

    var msg = "";

    if (newVal >= 20 && newVal < 40 && !done_20) {
        done_20 = true;
        Log("20% completed");
    } else if (newVal >= 40 && newVal < 60 && !done_40) {
        done_40 = true;
        Log("40% completed");
    } else if (newVal >= 60 && newVal < 80 && !done_60) {
        done_60 = true;
        Log("60% completed");
    } else if (newVal >= 80 && newVal < 100 && !done_80) {
        done_80 = true;
        Log("80% completed");
    } else if (newVal >= 100 && !done_100) {
        done_100 = true;
        Log("100% completed");
        progressBar.attr('class', 'progress-bar bg-success');
    }

    if (msg !== "") {
        Log(msg);
    }
}
function SetClientCount(newVal) {
    $('#clientCount').text(newVal);

    var msg = "";

    if (newVal == 1) {
        if (firstUser) {
            firstUser = false;
            msg = "First client connected!";
        }
    }
    else if (newVal % 10 == 0) {
        if (newVal == 0) {
            msg = "No clients connected."
        }
        else {
            msg = "Reached " + newVal + " clients.";
        }
    }

    if (msg !== "") {
        Log(msg);
    }
}

function Log(eventMsg) {
    var currentDate = new Date();
    var timeMsg = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();

    var finalMsg = "[" + timeMsg + "]: " + eventMsg;
    logList.append("<li class='list-group-item'>" + finalMsg + "</li>");

    var height = logDiv[0].scrollHeight;
    logDiv.scrollTop(height);
}

function CloseSocket() {
    console.log("Closing socket...");
    socket.close();
}