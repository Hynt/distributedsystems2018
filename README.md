# DSort

## What is this?
This is a browser based distributed sorting solution. Administration side can create arrays, open connections, track the progress. Clients can connect to the server to open up
their resources used for sorting

## Technical
Written in Java, using SparkJava as a web framework, mustache as a templating engine. 
Custom written task distribution to distribute the array to be sorted.

###Links:

Overleaf 
https://www.overleaf.com/1378919185pgdwknbjqxyc

Trello https://trello.com/b/tSzQ4k0G/parallel-sorting

Diagrams https://drive.google.com/file/d/1eYRtnYpU9qcpKCFw4GpynOZeTO7OCTb5/view?usp=sharing